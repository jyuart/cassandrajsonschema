const { tableData, UDTInfo, firstyEntry } = require ('./data')
const toJsonSchema = require('to-json-schema');


const convertCassandraToJsonTypes = function(cassandraType) {
    if(cassandraType.startsWith('set') || cassandraType.startsWith('list'))
        return handlingLists(cassandraType)

    else if(cassandraType.startsWith('map'))
        return handlingMaps(cassandraType)

    switch(cassandraType) {

        case 'boolean':
            return 'boolean'
        case 'ascii':
        case 'date':
        case 'uuid':
        case 'timeuuid':
        case 'text':
        case 'varchar':
        case 'inet':
        case 'time':
        case 'timestamp':
        case 'blob':
            return 'string'
        case 'bigint':
        case 'counter':
        case 'decimal':
        case 'double':
        case 'duration':
        case 'float':
        case 'int':
        case 'smallint':
        case 'variant':
            return 'number'
        default:
            return handlingUDTs(cassandraType)
            }
}

const handlingLists = function(cassandraType) {
    const setListType = cassandraType.substring(cassandraType.indexOf('<') + 1, cassandraType.length - 1);
    return {
        type: 'array',
        items: convertCassandraToJsonTypes(setListType)
    }
}

const handlingMaps = function(cassandraType) {
    const mapTypes = cassandraType.substring(cassandraType.indexOf('<') + 1, cassandraType.length - 1).split(', ');
    return {
        type: 'array',
        items: {
            type: 'object',
            properties: {
                'key': convertCassandraToJsonTypes(mapTypes[0]),
                'value': convertCassandraToJsonTypes(mapTypes[1])
            }
        }
    }
}

const handlingUDTs = (cassandraType) => {
        UDTInfo(cassandraType).then((row) => {
            if(row) {
                var typeName = row['type_name'];
                typeName = {
                    type: 'object',
                    properties: {}
                }
                const names = row['field_names'];
                const types = row['field_types'];
                names.forEach((name, index) => {
                    typeName.properties[name] =  { type: convertCassandraToJsonTypes(types[index]) }
                })
                return typeName
            }
        }
    )
}

const removeFrozen = function(cassandraType) {
    if (cassandraType.includes("frozen"))
        cassandraType = cassandraType.replace('frozen<', '').replace('>', '');
    return cassandraType;
}

const handleJsonField = function(tableName, columnName) {
        firstyEntry(tableName, columnName)
            .then((result) => {
                if(result) {
                    const textField = result[0][columnName];
                    try {
                        if(JSON.parse(textField))
                        {
                            return toJsonSchema(JSON.parse(textField));
                        }
                        else {
                            return 'text'
                        }
                    }
                    catch {}
                }
            })
}

const creatingSchema = (tableName) => tableData(tableName).then((row) => {
                        var finalSchema = {
                            $schema: 'http://json-schema.org/draft-04/schema#',
                            type: 'object',
                            titile: tableName,
                            properties: {}
                        }
                        row.forEach((row) => {
                        const columnName = row['column_name'];
                        var initialType = removeFrozen(row['type']);
                        const JsonType = convertCassandraToJsonTypes(initialType)
                        finalSchema.properties[columnName] = { type: JsonType }
                        })
                        return finalSchema
                        })
                        
module.exports = { creatingSchema }