const cassandra = require('cassandra-driver');
const config = require('./config');

const client = new cassandra.Client({
  contactPoints: [`${config.host}:${config.port}`],
  localDataCenter: config.localDataCenter,
  credentials: { 
    username: config.user,
    password: config.password
   }
});

const dbConnection = client.connect()
      .then(() => console.log("Connection established"),
      (err) =>  console.log(`Error ocurred while trying to connect\n${err}`))

module.exports = { cassandra, dbConnection, client }