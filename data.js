const { dbConnection, client } = require('./db');
const config = require('./config');

const tableData = (table_name) => dbConnection.then(() => client.execute(`SELECT column_name, type FROM system_schema.columns WHERE keyspace_name = '${config.keyspace_name}' AND table_name='${table_name}'`)
                        .then((result) => result.rows),
                        (err) => console.log("Error occurred while trying to get data"));

const UDTInfo = (type_name) => dbConnection.then(() => client.execute(`SELECT * FROM system_schema.types WHERE keyspace_name='${config.keyspace_name}' AND type_name='${type_name}';`)
                        .then((result) => result.rows[0]),
                        (err) => console.log("Error occurred while trying to get UDT data"));

const allTables = () => dbConnection.then(() => client.execute(`SELECT table_name FROM system_schema.tables WHERE keyspace_name = '${config.keyspace_name}';`)
                        .then((result) => result.rows.map(t => t.values()[0]),
                        (err) => console.log("Error occurred while trying to get all tables names")));

const firstyEntry = (table, column) => dbConnection.then(() => client.execute(`SELECT ${column} FROM ${config.keyspace_name}.${table} LIMIT 1`)
                        .then((result) => result.rows),
                        (err) => console.log("Error while getting data from table"));
                
module.exports = { tableData, UDTInfo, allTables, firstyEntry }