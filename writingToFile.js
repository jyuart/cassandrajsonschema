const { creatingSchema } = require('./convert')
const { allTables } = require('./data')
const fs = require('fs')

var i = 0
   
const startWriting = allTables().then((tables) => {
    tables.forEach((table) => {
        creatingSchema(table).then((result) => {
            fs.appendFile(`result${++i}.json`, JSON.stringify(result), () => console.log("Done"))
        })
    })
})

module.exports = startWriting;